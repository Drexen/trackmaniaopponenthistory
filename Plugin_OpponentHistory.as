#name "OpponentHistory"
#author "Drexen"
#category "Race"

#include "Icons.as"
#include "Time.as"
#include "Formatting.as"

const string k_keyMatchID = "matchID";
const string k_keyMatchMapName = "mapName";
const string k_keyMatchStartDate = "startDate";
const string k_keyTeamScoreZero = "teamScoreZero";
const string k_keyTeamScoreOne = "teamScoreOne";

const string k_keyMatches = "matches";
const string k_keyPlayers = "players";

const string k_keyPlayerName = "name";
const string k_keyPlayerID = "id";
const string k_keyRank = "rank";
const string k_keyScore = "score";
const string k_keyTeam = "team";
const string k_keyMatchmakingPointsBefore = "matchmakingPointsBefore";
const string k_keyMatchmakingPointsAfter = "matchmakingPointsAfter";

array<MatchPlayerInfo@> g_currentMatchPlayerInfos;
Json::Value g_opponentHistory = Json::Object();
dictionary g_allPlayersHistoryData;
dictionary g_allMapCountHistory;

class HistoricalPlayerInfo
{
	string ID;
	uint WinCount;
	uint LossCount;
	uint RankSum;
	uint64 LastSeen;
}

class MatchPlayerInfo
{
	string Name;
	string WinLossPlayedCount;
	string AverageRank;
	string LastSeenStr;
}

void Main()
{
	print("Initializing OpponentHistory ...");

	string path = IO::FromDataFolder("OpponentHistory.json");
	if (IO::FileExists(path) == true)
	{
		g_opponentHistory = Json::FromFile(path);
		print ("Loaded save data");
	}

	auto app = cast<CTrackMania>(GetApp());
	auto network = cast<CTrackManiaNetwork>(app.Network);

	CheckAllMatchesHistory(network);
	RebuildAllMatchesHistoryData();
	RebuildAllMapHistoryData();

	while (true)
	{
		auto serverInfo = cast<CGameCtnNetServerInfo>(network.ServerInfo);

		if (g_currentMatchPlayerInfos.Length == 0)
		{
			if (IsInMatchmaking(serverInfo))
			{
				uint numPlayers = network.PlayerInfos.Length;
				if (numPlayers < 6)
				{
					print ("Waiting for 6 players: " + numPlayers);
				}
				else
				{
					print ("Started match");
					SetupCurrentMatchPlayerInfos(network, serverInfo);
				}
			}
			else
			{
				print ("Waiting for match");
			}
		}
		else
		{
			if (IsInMatchmaking(serverInfo) == false)
			{
				// left match
				CheckAllMatchesHistory(network);
				RebuildAllMatchesHistoryData();
				RebuildAllMapHistoryData();
				g_currentMatchPlayerInfos = {};
			}
			else
			{
				SetupCurrentMatchPlayerInfos(network, serverInfo);
			}
		}

		sleep(20000);
	}
}

/// Take our raw saved json data and precalculate into something more useable
void RebuildAllMatchesHistoryData()
{
	g_allPlayersHistoryData = {};
	if (g_opponentHistory.HasKey(k_keyMatches) == false)
	{
		return;
	}

	auto allMatches = g_opponentHistory[k_keyMatches];
	for (uint i = 0; i < allMatches.Length; ++i)
	{
		RebuildHistoryForMatch(allMatches[i]);
	}
}

void RebuildHistoryForMatch(Json::Value matchJson)
{
	bool didTeamZeroWin = (matchJson["teamScoreZero"] == 5);

	auto matchPlayersJson = matchJson["players"];
	for (uint i = 0; i < matchPlayersJson.Length; ++i)
	{
		auto playerJson = matchPlayersJson[i];

		string playerID = playerJson["id"];

		HistoricalPlayerInfo@ playerData = HistoricalPlayerInfo();
		if (g_allPlayersHistoryData.Exists(playerID) == true)
		{
			playerData = cast<HistoricalPlayerInfo@>(g_allPlayersHistoryData[playerID]);
		}

		playerData.ID = playerID;

		int playerTeam = playerJson["team"];
		if ((didTeamZeroWin && playerTeam == 0) ||
			(!didTeamZeroWin && playerTeam == 1))
		{
			playerData.WinCount = playerData.WinCount + 1;
		}
		else
		{
			playerData.LossCount = playerData.LossCount + 1;
		}

		playerData.RankSum = playerData.RankSum + playerJson["rank"];

		uint64 matchStartDate = Text::ParseUInt64(matchJson["startDate"]);
		if (playerData.LastSeen == 0 || playerData.LastSeen < matchStartDate)
		{
			playerData.LastSeen = matchStartDate;
		}

		g_allPlayersHistoryData[playerID] = playerData;
	}
}

void RenderInterface()
{
	auto app = cast<CTrackMania>(GetApp());

	if (app.RootMap is null)
	{
		return;
	}

	auto network = cast<CTrackManiaNetwork>(app.Network);
	auto serverInfo = cast<CGameCtnNetServerInfo>(network.ServerInfo);

	if (IsInMatchmaking(serverInfo) == false)
	{
		return;
	}

	//UI::SetNextWindowPos(0, 24);
	UI::SetNextWindowSize(420, 230, UI::Cond::Always);
	UI::Begin("OpponentHistory", UI::WindowFlags::NoCollapse | UI::WindowFlags::NoResize);
	UI::Columns(4, "");

	UI::Text("User"); UI::NextColumn();
	UI::Text("W/L [Total]"); UI::NextColumn();
	UI::Text("Avg. Rank"); UI::NextColumn();
	UI::Text("Seen"); UI::NextColumn();

	for (uint i = 0; i < g_currentMatchPlayerInfos.Length; i++)
	{
		auto info = g_currentMatchPlayerInfos[i];
		UI::Separator();
		UI::Text(info.Name); UI::NextColumn();
		UI::Text(info.WinLossPlayedCount); UI::NextColumn();
		UI::Text(info.AverageRank); UI::NextColumn();
		UI::Text(info.LastSeenStr); UI::NextColumn();
	}

	UI::End();
}

void RebuildAllMapHistoryData()
{
	g_allMapCountHistory = {};

	if (g_opponentHistory.HasKey(k_keyMatches) == false)
	{
		return;
	}

	auto matches = g_opponentHistory[k_keyMatches];
	for (uint i = 0; i < matches.Length; ++i)
	{
		auto match = matches[i];
		string map = match[k_keyMatchMapName];

		uint playCount = 1;
		if (g_allMapCountHistory.Exists(map))
		{
			playCount = 1 + uint(g_allMapCountHistory[map]);
		}

		g_allMapCountHistory[map] = playCount;
	}
}

void RenderMenu()
{
	if (UI::BeginMenu("\\$9cf" + Icons::InfoCircle + "\\$z Match history") == false)
	{
		return;
	}

	if (!(g_allMapCountHistory is null) &&
		g_allMapCountHistory.GetSize() > 0 &&
		UI::BeginMenu("Map Count"))
	{
		array<string> keys = g_allMapCountHistory.GetKeys();
		for (uint i = 0; i < keys.Length; ++i)
		{
			string key = keys[i];
			uint playCount = uint(g_allMapCountHistory[key]);
			UI::MenuItem(key + ": " + playCount);
		}

		UI::EndMenu();
	}

	UI::EndMenu();
}

void SetupCurrentMatchPlayerInfos(CTrackManiaNetwork@ network, CGameCtnNetServerInfo@ serverInfo)
{
	print ("Refreshing info");

	g_currentMatchPlayerInfos = {};

	for (uint i = 0; i < network.PlayerInfos.Length; i++)
	{
		auto playerInfo = cast<CTrackManiaPlayerInfo>(network.PlayerInfos[i]);
		if (playerInfo is null || playerInfo.Login == serverInfo.ServerLogin)
		{
			continue;
		}

		MatchPlayerInfo@ info = GetMatchPlayerInfo(playerInfo.WebServicesUserId, playerInfo.Name);
		g_currentMatchPlayerInfos.InsertLast(info);
	}
}

MatchPlayerInfo GetMatchPlayerInfo(string accountID, string accountName)
{
	MatchPlayerInfo@ info = MatchPlayerInfo();
	info.Name = accountName;

	if (g_allPlayersHistoryData.Exists(accountID) == false)
	{
		return info;
	}

	HistoricalPlayerInfo@ playerData = cast<HistoricalPlayerInfo>(g_allPlayersHistoryData[accountID]);

	uint numMatchesSeen = playerData.WinCount + playerData.LossCount;
	info.WinLossPlayedCount = "" + playerData.WinCount + "/" + playerData.LossCount + "  [" + numMatchesSeen + "]";

	float averageRank = (float(playerData.RankSum) / float(numMatchesSeen));
	string averageRankStr = Text::Format("%.2f", averageRank);
	info.AverageRank = averageRankStr;

	// string timeLastSeenStr = Json::Write(playerData[k_keyLastSeenTime]).Replace("\"", "");
	// uint64 timeLastSeen = Text::ParseUInt64(timeLastSeenStr);
	info.LastSeenStr = GetLastSeenStr(playerData.LastSeen);

	return info;
}

string GetLastSeenStr(uint64 timeLastSeen)
{
	double secondsAgo = Time::Stamp - timeLastSeen;
	double minutesAgo = secondsAgo / 60;
	if (minutesAgo < 60)
	{
		int minutes = minutesAgo;
		return "" + minutes + "m";
	}

	double hoursAgo = minutesAgo / 60;
	if (hoursAgo < 24)
	{
		int hours = hoursAgo;
		return "" + hours + "h";
	}

	double daysAgo = hoursAgo / 24;
	int days = daysAgo;
	return "" + days + "d";
}

bool IsInMatchmaking(CGameCtnNetServerInfo@ serverInfo)
{
	if (serverInfo is null || serverInfo.ServerLogin == "")
	{
		return false;
	}

	if (serverInfo.ModeName != "TM_Teams_Matchmaking_Online")
	{
		return false;
	}

	return true;
}

void CheckAllMatchesHistory(CTrackManiaNetwork@ network)
{
	string accountID = network.PlayerInfo.WebServicesUserId;

	bool stopFetchingHistory = false;
	for (uint j = 0; j < 10; j++)
	{
		print ("Getting match history page: " + j);
		string matchHistoryResponse = SendJSONRequest(Net::HttpMethod::Get, "https://trackmania.io/api/player/" + accountID + "/matches/2/" + j);

		Json::Value matchHistoryJson = ResponseToJSON(matchHistoryResponse, Json::Type::Object);
		if (matchHistoryJson.GetType() == Json::Type::Null ||
			matchHistoryJson.HasKey(k_keyMatches) == false ||
			matchHistoryJson[k_keyMatches].Length == 0)
		{
			print ("Failed to find matches");
			return;
		}

		Json::Value matches = matchHistoryJson[k_keyMatches];
		print ("Found matches: " + matches.Length);

		for (uint i = 0; i < matches.Length; i++)
		{
			stopFetchingHistory = CheckMatchHistory(accountID, matches[i]);
			if (stopFetchingHistory == true)
			{
				print ("Stopping match history fetch");
				break;
			}
		}

		if (stopFetchingHistory == true)
		{
			break;
		}
	}

	SaveAllMatchHistory();
}

void SaveAllMatchHistory()
{
	// Gets the absolute path for a file in the data folder. This is typically C:\Users\Username\OpenplanetNext
	string path = IO::FromDataFolder("OpponentHistory.json");

	print ("Saved to: " + path);

	Json::ToFile(path, g_opponentHistory); // Serializes a Json value tree to a file.
}

/// Returns whether we should stop fetching subsequent match history
bool CheckMatchHistory(string myAccountID, Json::Value matchJson)
{
	if (matchJson["typename"] != "3v3")
	{
		return false;
	}

	string lid = matchJson["lid"];

	if (IsMatchAlreadyParsed(lid) == true)
	{
		print ("Match already parsed");
		return true;
	}

	string matchStatusResponse = SendJSONRequest(Net::HttpMethod::Get, "https://trackmania.io/api/match/" + lid);
	Json::Value matchStatus = ResponseToJSON(matchStatusResponse, Json::Type::Object);

	if (matchStatus.GetType() == Json::Type::Null || !matchStatus.HasKey(k_keyPlayers))
	{
		print ("Failed to get valid info for match: " + lid);
		return true;
	}

	Json::Value players = matchStatus[k_keyPlayers];
	if (players.Length != 6)
	{
		print ("Bad number of players in this match: " + players.Length);
		return false;
	}

	RecordMatchHistory(lid, myAccountID, matchJson, matchStatus);
	return false;
}

bool IsMatchAlreadyParsed(string lid)
{
	if (g_opponentHistory.HasKey(k_keyMatches) == false)
	{
		return false;
	}

	auto matches = g_opponentHistory[k_keyMatches];

	for (uint i = 0; i < matches.Length; ++i)
	{
		auto match = matches[i];
		if (match[k_keyMatchID] == lid)
		{
			return true;
		}
	}

	return false;
}

void RecordMatchHistory(string matchID, string myAccountID, Json::Value matchJson, Json::Value matchStatus)
{
	print ("Parsing match: " + matchID);

	if (g_opponentHistory.HasKey(k_keyMatches) == false)
	{
		g_opponentHistory[k_keyMatches] = Json::Array();
	}

	uint64 matchStartDate = matchStatus["startdate"];
	string matchStartDateStr = Text::Format("%u", matchStartDate);

	auto matchInfo = Json::Object();
	matchInfo[k_keyMatchID] = matchID;
	matchInfo[k_keyMatchMapName] = GetMapNameFromMatchStatus(matchStatus);
	matchInfo[k_keyMatchStartDate] = matchStartDateStr;

	matchInfo[k_keyTeamScoreZero] = GetTeamScoreFromMatchStatus(0, matchStatus);
	matchInfo[k_keyTeamScoreOne] = GetTeamScoreFromMatchStatus(1, matchStatus);

	matchInfo[k_keyPlayers] = GetAllMatchPlayersInfo(matchStatus);

	g_opponentHistory[k_keyMatches].Add(matchInfo);
}

Json::Value GetAllMatchPlayersInfo(Json::Value matchStatus)
{
	Json::Value data = Json::Array();

	if (matchStatus.HasKey("players") == false)
	{
		print ("MatchStatus has no key: players");
		return data;
	}

	auto players = matchStatus["players"];

	for (uint i = 0; i < 6; ++i)
	{
		data.Add(GetMatchPlayerInfo(players[i]));
	}

	return data;
}

Json::Value GetMatchPlayerInfo(Json::Value data)
{
	auto output = Json::Object();

	if (data.HasKey("player") == false)
	{
		print ("Match player data has no key: player");
		return output;
	}
	auto playerData = data["player"];

	output[k_keyPlayerName] = playerData["name"];
	output[k_keyPlayerID] = playerData["id"];
	output[k_keyRank] = data["rank"];
	output[k_keyScore] = data["score"];
	output[k_keyTeam] = data["team"];
	output[k_keyMatchmakingPointsBefore] = data["matchmakingpoints"]["before"];
	output[k_keyMatchmakingPointsAfter] = data["matchmakingpoints"]["after"];

	return output;
}

string GetMapNameFromMatchStatus(Json::Value matchStatus)
{
	if (matchStatus.HasKey("maps") == false)
	{
		print ("MatchStatus has no key: maps");
		return "";
	}

	auto maps = matchStatus["maps"];
	auto firstMap = maps[0];
	if (firstMap.HasKey("file") == false)
	{
		print ("FirstMap has no key: file");
		return "";
	}

	string mapName = Json::Write(firstMap["file"]);
	return mapName;
}

int GetTeamScoreFromMatchStatus(int teamIndex, Json::Value matchStatus)
{
	if (matchStatus.HasKey("teams") == false)
	{
		print ("MatchStatus has no key: teams");
		return -1;
	}

	auto teams = matchStatus["teams"];
	auto team = teams[teamIndex];
	if (team.HasKey("score") == false)
	{
		print ("TeamInfo has no key: score");
		return -1;
	}

	int teamScore = team["score"];
	return teamScore;
}

// NETWORK
string SendNadeoRequest(const Net::HttpMethod Method, const string &in URL, string Body = "") {
    dictionary@ Headers = dictionary();
    auto app = cast<CTrackMania>(GetApp());
    Headers["Accept"] = "application/json";
    Headers["Content-Type"] = "application/json";
    Headers["Authorization"] = "nadeo_v1 t=" + app.ManiaPlanetScriptAPI.Authentication_Token;
    return SendHTTPRequest(Method, URL, Body, Headers);
}

string SendJSONRequest(const Net::HttpMethod Method, const string &in URL, string Body = "") {
    dictionary@ Headers = dictionary();
    Headers["Accept"] = "application/json";
    Headers["Content-Type"] = "application/json";
    return SendHTTPRequest(Method, URL, Body, Headers);
}

string SendHTTPRequest(const Net::HttpMethod Method, const string &in URL, const string &in Body, dictionary@ Headers) {
    Net::HttpRequest req;
    req.Method = Method;
    req.Url = URL;
    @req.Headers = Headers;
    //req.Headers["User-Agent"] = "Openplanet / Net::HttpRequest / AFK Queue Tool v" + PluginVersion;
    req.Body = Body;
    req.Start();
    while (!req.Finished()) {
        yield();
    }
    //@BETA
    //print(req.String());
    return req.String();
}

Json::Value ResponseToJSON(const string &in HTTPResponse, Json::Type ExpectedType) {
    Json::Value ReturnedObject;
    try {
        ReturnedObject = Json::Parse(HTTPResponse);
    } catch {
        //Error(ErrorType::Error, "ResponseToJSON", "JSON Parsing of string failed!", HTTPResponse);
		print("Exception parsing response");
    }

    if (ReturnedObject.GetType() != ExpectedType) {
        //Error(ErrorType::Warn, "ResponseToJSON", "Unexpected JSON Type returned", HTTPResponse);
		print("Unexpected json type returned" + ReturnedObject.GetType());
        return ReturnedObject;
    }
    return ReturnedObject;
}
